import React from 'react';
import { Redirect } from 'react-router';

function CreateEvent(props) {

    if(props.event !== null) {
        return <Redirect to={'/events/' + props.event.slug}/>
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        props.createEvent()
    }

    return (
        <>
            <h1>Créez votre évènement</h1>
            <form onSubmit={handleSubmit}>
                <input type="text" onChange={event => props.changeInputEvent(event.target.value)}/>
                <button>En avant Guingamp !</button>
            </form>
        </>
    );
}

export default CreateEvent;