import { connect } from 'react-redux';
import CreateEvent from './CreateEvent';
import { fetchCreateEvent, changeInputEvent } from '../../../actions/actions';

// Envoyer des données dans les props
const mapStateToProps = state => ({
    changeInputEvent: state.front.inputEvent,
    event: state.front.event,
    inputEvent: state.front.inputEvent,
})

// Envoyer des fonctions dans les props
const mapDispatchToProps = dispatch => ({
    createEvent: () => dispatch(fetchCreateEvent()),
    changeInputEvent: (slug) => dispatch(changeInputEvent(slug)),
    
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(CreateEvent)