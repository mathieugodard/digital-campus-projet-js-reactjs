import React, {useEffect} from 'react';
import { Redirect } from 'react-router';

function EventList(props) {

    const {fetchExpenses} = props;
    useEffect(() => fetchExpenses(), [fetchExpenses])

    // console.log('yep')

    const {expenses} = props;
    const expenseJsx = expenses.map(expense => 
        <div className="card" key={expense.id}>
            <div className="card-body">
                <h5 className="card-title">{expense.title}</h5>
                <h6 className="card-subtitle mb-2 text-muted">{expense.user} : {expense.amount} € le {new Date(expense.createdAt).toLocaleDateString()} à {new Date(expense.createdAt).toLocaleTimeString('fr-FR')}</h6>
            </div>
        </div>
    );
    
    const handleSubmit = (event) => {
        event.preventDefault();
        props.fetchCreateExpense()
    }

    if(props.event === null) {
        return <Redirect to='/join'/>
    }

    return (
        <>
            <h1>{props.event.name}</h1>
            <button className="btn btn-danger mr-md-3" onClick={() => props.deleteEvent()}>Supprimer l'évènement</button>
            <button className="btn btn-dark mr-md-3" onClick={() => props.initializeEvent(null)}>Fermer</button>

            <h3>Ajouter une dépense</h3>
            <form action="" onSubmit={handleSubmit}>
                <div>
                    <label htmlFor="c20">Titre de la dépense &nbsp; </label>
                    <input id="c20" type="text" name="title" placeholder="Saisissez la nature de la dépense" maxLength="255" size="100" width="250" value={props.title} required="" onChange={event => props.addTitleExpense(event.target.value)}/>
                </div>

                <div>
                    <label htmlFor="c30">Participant.e &nbsp;</label>
                    <input id="c30" type="text" name="user" placeholder="Saisissez votre blase" maxLength="100" size="100" width="250" required="" value={props.user} onChange={event => props.addUserExpense(event.target.value)}/>
                </div>

                <div>
                    <label htmlFor="c130">Montant de la dépense &nbsp;</label>
                    <input id="c130" type="number" name="amount" min="0" value={props.amount} onChange={event => props.addAmountExpense(event.target.value)}/>
                </div>
                <div>
                    <input type="submit" value="Ajouter"></input>
                </div>
                
            </form>
            
            <h3>Liste des dépenses :</h3>
            {expenseJsx}
        </>
    );
}

export default EventList;