import { connect } from 'react-redux';
import EventList from './EventList';
import { 
  initializeEvent, 
  fetchExpenses, 
  fetchDeleteEvent, 
  addTitleExpense,
  addUserExpense,
  addAmountExpense,
  fetchCreateExpense,
} from '../../../actions/actions'

// Envoyer des données dans les props
const mapStateToProps = state => ({
  event: state.front.event,
  expenses: state.front.expenses,
  title: state.front.expenseForm.title,
  user: state.front.expenseForm.user,
  amount: state.front.expenseForm.amount,
})

// Envoyer des fonctions dans les props
const mapDispatchToProps = dispatch => ({
  initializeEvent: (initEvent) => dispatch(initializeEvent(initEvent)),
  fetchExpenses: () => dispatch(fetchExpenses()),
  deleteEvent: () => dispatch(fetchDeleteEvent()),
  addTitleExpense: (title) => dispatch(addTitleExpense(title)),
  addUserExpense: (user) => dispatch(addUserExpense(user)),
  addAmountExpense: (amount) => dispatch(addAmountExpense(amount)),
  fetchCreateExpense: () => dispatch(fetchCreateExpense()),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(EventList)