import Join from './Join';
import { connect } from 'react-redux';
import { changeInputEvent, fetchEvents } from '../../../actions/actions'

// Envoyer des données dans les props
const mapStateToProps = state => ({

    // Va chercher le state actualisé dans le reducer
    inputEvent: state.front.inputEvent,
    event: state.front.event,
    error: state.front.error.event,
})

// Envoyer des fonctions dans les props
const mapDispatchToProps = dispatch => ({

    // Appel de la fonction qui modifie l'état du state
    changeInputEvent: (slug) => dispatch(changeInputEvent(slug)),

    // Appel de la fonction qui va chercher les données
    fetchEvents: () => dispatch(fetchEvents()),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Join)
