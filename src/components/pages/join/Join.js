import React from 'react';
import { Redirect } from 'react-router';

function Join(props) {

    if(props.event !== null) {
        return <Redirect to={'/events/' + props.inputEvent}/>
    }

    let error = null;
    if(props.error) {
        error =  <p className="alert alert-danger">Le slug n'existe pô, tu bluffes Martoni.</p>
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        props.fetchEvents()
    }
    
    return (
        <>
            <h1>Rejoindre un évènement</h1>
            <p>Indiquez le slug de l'évènement (ex : conference-sur-la-methode-pls-pour-etudiants-en-developpement-web) :</p>
            {error}
            <form onSubmit={handleSubmit}>
            
                <input type="text" value={props.inputEvent} onChange={event => props.changeInputEvent(event.target.value)} placeholder="slug"/>
                <input type="submit" value="Join"></input>
            </form>
        </>
    );
}

export default Join;