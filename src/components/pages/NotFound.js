import React from 'react';

function NotFound(props) {
    return (
        <div>
            <p>Bonjour, la page que vous avez demandé n'est pas attribuée, merci de revenir en arrière.</p>
        </div>
    );
}

export default NotFound;