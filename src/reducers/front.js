import { 
    CHANGE_INPUT_EVENT, 
    INITIALIZE_EVENT,
    FETCH_EVENTS_PENDING,
    FETCH_EVENTS_SUCCESS,
    FETCH_EVENTS_FAILURE,
    FETCH_EXPENSES_PENDING,
    FETCH_EXPENSES_SUCCESS,
    FETCH_EXPENSES_FAILURE,
    FETCH_CREATE_EVENT_PENDING,
    FETCH_CREATE_EVENT_SUCCESS,
    FETCH_CREATE_EVENT_FAILURE,
    FETCH_DELETE_EVENT_PENDING,
    FETCH_DELETE_EVENT_SUCCESS,
    FETCH_DELETE_EVENT_FAILURE,
    ADD_TITLE_EXPENSE,
    ADD_USER_EXPENSE,
    ADD_AMOUNT_EXPENSE,
    FETCH_CREATE_EXPENSE_PENDING,
    FETCH_CREATE_EXPENSE_SUCCESS,
    FETCH_CREATE_EXPENSE_FAILURE,
} from '../actions/actions'

const initialState = {
    inputEvent:'',  // Initialisation de l'input du formulaire d'accès
    event: null,    // Initialisation de l'évènement
    error: {event: null, expenses: null},
    expenses: [],
    expenseForm: {
        title:'',
        user:'',
        amount:'', 
        paid:true, 
        createdAt:(new Date()).toLocaleDateString(), 
        category:'/categories/1', 
        event:'/events/conference-sur-la-methode-pls-pour-etudiants-en-developpement-web'
    }
};

function front(state = initialState, action) {
    switch (action.type) {
        // Retourne un nouveau state en mettant à jour InputEvent :
        case CHANGE_INPUT_EVENT:
            return {...state, inputEvent: action.payload };

        case INITIALIZE_EVENT:
            return {...state, event: action.payload };

        case FETCH_EVENTS_PENDING:
            return { ...state };

        case FETCH_EVENTS_SUCCESS:
            return { ...state, event: action.payload };

        case FETCH_EVENTS_FAILURE:
            return { ...state, error: { ...state.error, event: action.payload } };

        case FETCH_EXPENSES_PENDING:
            return { ...state };

        case FETCH_EXPENSES_SUCCESS:
            return { ...state, expenses: action.payload };

        case FETCH_EXPENSES_FAILURE:
            return { ...state, error: { ...state.error, expenses: action.payload } };

        case FETCH_CREATE_EVENT_PENDING:
            return { ...state };

        case FETCH_CREATE_EVENT_SUCCESS:
            return { ...state, event: action.payload };

        case FETCH_CREATE_EVENT_FAILURE:
            return { ...state, error: { ...state.error, event: action.payload } };

        case FETCH_DELETE_EVENT_PENDING:
            return { ...state };

        case FETCH_DELETE_EVENT_SUCCESS:
            return { ...state, event: null };

        case FETCH_DELETE_EVENT_FAILURE:
            return { ...state, error: { ...state.error, event: action.payload } };

        case ADD_TITLE_EXPENSE:
        return {...state, expenseForm: {...state.expenseForm, title: action.payload} };

        case ADD_USER_EXPENSE:
        return {...state, expenseForm: {...state.expenseForm, user: action.payload } };

        case ADD_AMOUNT_EXPENSE:
        return {...state, expenseForm: {...state.expenseForm, amount: action.payload } };

        case FETCH_CREATE_EXPENSE_PENDING:
            return { ...state };

        case FETCH_CREATE_EXPENSE_SUCCESS:
            return { ...state, event: action.payload };

        case FETCH_CREATE_EXPENSE_FAILURE:
            return { ...state, error: { ...state.error, event: action.payload } };
        
        default:
            return state;
    }
}

export default front;