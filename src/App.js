import React from "react";
import {Switch, Route} from "react-router-dom";
import NavBar from "./components/layout/NavBar";
import HomePage from "./components/pages/Homepage";
import NotFound from "./components/pages/NotFound";
import JoinContainer from "./components/pages/join/JoinContainer";
import CreateEventContainer from "./components/pages/createEvent/CreateEventContainer";
import EventListContainer from "./components/pages/events/EventListContainer";
import './App.scss';

function App() {
  return (
    <React.Fragment>
      <NavBar/>
      <Switch>
        <Route exact  path='/'        component={HomePage} />
        <Route exact  path='/join'    component={JoinContainer} />
        <Route        path='/events/' component={EventListContainer} />
        <Route exact  path='/create'  component={CreateEventContainer} />
        <Route                        component={NotFound} />
      </Switch>
    </React.Fragment>
  );
}

export default App;
