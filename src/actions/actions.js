// Action sur la modification de la valeur de l'input du composant Join.js
export const CHANGE_INPUT_EVENT = 'CHANGE_INPUT_EVENT';
export function changeInputEvent(slug) {
  return { type: CHANGE_INPUT_EVENT, payload: slug };
}

// Initialiser l'event pour fermer l'évènement
export const INITIALIZE_EVENT = 'INITIALIZE_EVENT';
export function initializeEvent(initEvent) {
  return { type: INITIALIZE_EVENT, payload: initEvent };
}

// Action pour récupérer les données de l'évènement, 
// à partir du slug inscrit dans l'input du composant Join.js
export function fetchEvents() {
    return (dispatch, getState) => {
      const slug = getState().front.inputEvent;

      dispatch(fetchEventsPending());
      fetch(process.env.REACT_APP_API_URL + '/events/' + slug)
        .then(response => response.json())
        .then(data => {
          if(data['@type'] === "hydra:Error") {
            return dispatch(fetchEventsFailure(data))
          }
          return dispatch(fetchEventsSuccess(data))
        })
        .catch(err => dispatch(fetchEventsFailure(err)))
    }
  }

export const FETCH_EVENTS_PENDING = 'FETCH_EVENTS_PENDING';
function fetchEventsPending() {
  return { type: FETCH_EVENTS_PENDING };
}

export const FETCH_EVENTS_SUCCESS = 'FETCH_EVENTS_SUCCESS';
function fetchEventsSuccess(event) {
  return { type: FETCH_EVENTS_SUCCESS, payload: event};
}

export const FETCH_EVENTS_FAILURE = 'FETCH_EVENTS_FAILURE';
function fetchEventsFailure(error) {
  return { type: FETCH_EVENTS_FAILURE, payload: error };
}


export function fetchExpenses() {
  return (dispatch, getState) => {
    const slug = getState().front.inputEvent;

    dispatch(fetchExpensesPending());
    fetch(process.env.REACT_APP_API_URL + '/events/' + slug + '/expenses')
      .then(response => response.json())
      .then(data => {
        if(data['@type'] === "hydra:Error") {
          return dispatch(fetchExpensesFailure(data))
        }
        return dispatch(fetchExpensesSuccess(data['hydra:member']))
      })
      .catch(err => dispatch(fetchExpensesFailure(err)))
  }
}

export const FETCH_EXPENSES_PENDING = 'FETCH_EXPENSES_PENDING';
function fetchExpensesPending() {
return { type: FETCH_EXPENSES_PENDING };
}

export const FETCH_EXPENSES_SUCCESS = 'FETCH_EXPENSES_SUCCESS';
function fetchExpensesSuccess(expense) {
return { type: FETCH_EXPENSES_SUCCESS, payload: expense};
}

export const FETCH_EXPENSES_FAILURE = 'FETCH_EXPENSES_FAILURE';
function fetchExpensesFailure(error) {
return { type: FETCH_EXPENSES_FAILURE, payload: error };
}




export function fetchCreateEvent() {
  return (dispatch, getState) => {
    const state = getState();

    dispatch(fetchCreateEventPending());

    fetch(process.env.REACT_APP_API_URL + '/events', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ name: state.front.inputEvent, expenses: [] })
    })
        .then(response => response.json())
        .then(data => dispatch(fetchCreateEventSuccess(data)))
        .catch(err => dispatch(fetchCreateEventFailure(err)))
  }
}

export const FETCH_CREATE_EVENT_PENDING = 'FETCH_CREATE_EVENT_PENDING';
function fetchCreateEventPending() {
return { type: FETCH_CREATE_EVENT_PENDING };
}

export const FETCH_CREATE_EVENT_SUCCESS = 'FETCH_CREATE_EVENT_SUCCESS';
function fetchCreateEventSuccess(event) {
return { type: FETCH_CREATE_EVENT_SUCCESS, payload: event};
}

export const FETCH_CREATE_EVENT_FAILURE = 'FETCH_CREATE_EVENT_FAILURE';
function fetchCreateEventFailure(error) {
return { type: FETCH_CREATE_EVENT_FAILURE, payload: error };
}





export function fetchDeleteEvent() {
  return (dispatch, getState) => {
    const slug = getState().front.inputEvent;

    dispatch(fetchDeleteEventPending());
    fetch(process.env.REACT_APP_API_URL + '/events/' + slug, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }})
      .then(response => response.json())
      .then(data => {
        if(data['@type'] === "hydra:Error") {
          return dispatch(fetchDeleteEventFailure(data))
        }
        return dispatch(fetchDeleteEventSuccess(data))
      })
      .catch(err => dispatch(fetchDeleteEventFailure(err)))
  }
}

export const FETCH_DELETE_EVENT_PENDING = 'FETCH_DELETE_EVENT_PENDING';
function fetchDeleteEventPending() {
return { type: FETCH_DELETE_EVENT_PENDING };
}

export const FETCH_DELETE_EVENT_SUCCESS = 'FETCH_DELETE_EVENT_SUCCESS';
function fetchDeleteEventSuccess(event) {
return { type: FETCH_DELETE_EVENT_SUCCESS, payload: event};
}

export const FETCH_DELETE_EVENT_FAILURE = 'FETCH_DELETE_EVENT_FAILURE';
function fetchDeleteEventFailure(error) {
return { type: FETCH_DELETE_EVENT_FAILURE, payload: error };
}




// Modification des states pour le formulaire d'ajout d'une dépense
export const ADD_TITLE_EXPENSE = 'ADD_TITLE_EXPENSE';
export function addTitleExpense(title) {
  return { type: ADD_TITLE_EXPENSE, payload: title };
}

export const ADD_USER_EXPENSE = 'ADD_USER_EXPENSE';
export function addUserExpense(user) {
  return { type: ADD_USER_EXPENSE, payload: user };
}

export const ADD_AMOUNT_EXPENSE = 'ADD_AMOUNT_EXPENSE';
export function addAmountExpense(amount) {
  return { type: ADD_AMOUNT_EXPENSE, payload: amount };
}




export function fetchCreateExpense() {
  return (dispatch, getState) => {
    const slug = getState().front.inputEvent;
    const expenseForm = getState().front.expenseForm;

    dispatch(fetchCreateExpensePending());

    fetch(process.env.REACT_APP_API_URL + '/expenses', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(expenseForm)
    })
        .then(response => response.json())
        .then(data => dispatch(fetchCreateExpenseSuccess(data)))
        .catch(err => dispatch(fetchCreateExpenseFailure(err)))
  }
}

export const FETCH_CREATE_EXPENSE_PENDING = 'FETCH_CREATE_EXPENSE_PENDING';
function fetchCreateExpensePending() {
return { type: FETCH_CREATE_EXPENSE_PENDING };
}

export const FETCH_CREATE_EXPENSE_SUCCESS = 'FETCH_CREATE_EXPENSE_SUCCESS';
function fetchCreateExpenseSuccess(expense) {
return { type: FETCH_CREATE_EXPENSE_SUCCESS, payload: expense};
}

export const FETCH_CREATE_EXPENSE_FAILURE = 'FETCH_CREATE_EXPENSE_FAILURE';
function fetchCreateExpenseFailure(error) {
return { type: FETCH_CREATE_EXPENSE_FAILURE, payload: error };
}